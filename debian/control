Source: radsecproxy
Section: net
Priority: optional
Maintainer: Sven Hartge <sven@svenhartge.de>
Build-Depends: debhelper-compat (= 13),
               libssl-dev,
               nettle-dev
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://radsecproxy.github.io/
Vcs-Browser: https://salsa.debian.org/debian/radsecproxy
Vcs-Git: https://salsa.debian.org/debian/radsecproxy.git

Package: radsecproxy
Architecture: any
Depends: adduser,
         ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Provides: radius-server
Description: RADIUS protocol proxy supporting RadSec
 radsecproxy is a generic RADIUS proxy that in addition to usual RADIUS UDP
 transport also supports TLS (RadSec). It aims to be flexible while at the same
 time small in size and memory footprint, efficient and easy to configure.
 .
 It can be useful as a proxy on site boundaries or in other complex RADIUS
 routing topologies. It supports both IPv4 and IPv6.
